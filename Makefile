analyzer: parse_log.c parse_log.h crc.c crc.h get_hamming_distance.c get_hamming_distance.h Makefile;
	gcc -static -O4 -o parse_log parse_log.c fingerprinting_buffer.c -lm
	gcc -static -O4 -o get_hamming_distance get_hamming_distance.c crc.c -lm
	gcc -static -O4 -o crc32 crc32.c crc.c -lm
	gcc -static -O4 -o check_collisions check_collisions.c crc.c fletchers.c -lm
clean:
	rm get_hamming_distance parse_log crc32
