#What is this

Utilities for parsing and analyzing text logs produced from the adl simulator. 

parse_log creates a binary log of the data that is being fingerprinted (e.g. write addresses, write data)

get_hamming_distance compares two binary logs and, given a block size, compares each block between the two files
and calculates the hamming distance (number of bits that differ), as well as the CRC of each block

#Compiling the code

Refer to COMPILE

#Usage

    parse_log [adl.log] [output.bin]

    get_hamming_distance [log1.bin] [log2.bin] [block size, in bytes]

`parse_log` can also take stdin as the first argument, in which case it reads a log file from standard input, for example

    parse_log stdin [output.bin] < [adl.log]
    cat [adl.log] | parse_log stdin [output.bin]

Additionally, `parse_log` can take as 3rd and 4th argument the cycle at which an error is inserted, and a reference error-free
binary log. This is used to determine the error latency.

    parse_log [adl.log] [output.bin] [cycle of error insertion] [nofault.bin]

`get_hamming_distance` can also take the CRC polynomial (reverse bit order) and the byte at which the error was inserted as arguments. 

    get_hamming_distance [log1.bin] [log2.bin] [block size, in bytes] [polynomial] [inserted at byte]

#Return Values

`parse_log` prints the latency, instruction at byte and required input buffer capacity (in number of 32 bit chunks). These results are useful when parsing. The latency here is the latency in terms of clock cycles, but the instruction at byte is needed to find the actual latency in terms of blovks

    [latency] [instruction at byte] [capacity]

`get_hamming_distance` prints a set of comma separated values - Polynomial,Block Size(bytes),CRC1,CRC2,Latency(Blocks),First Block Hamming Distance(bits),Total Hamming Distance(bits),Error Missed(First),Error Missed(Global)

 
