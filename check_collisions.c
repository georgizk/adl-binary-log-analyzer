#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "get_hamming_distance.h"
#include "crc.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
void interleave (void* buf,unsigned int bytes);
uint64_t interleave_64(uint64_t d);
int main(int argc, char *argv[]) {
    if (argc < 3){
        printf("Usage: %s [binary file 1] [binary file 2]\n", argv[0]);
        return -1;
    }
    int block_sizes[] = {1512,4092,32760}; 
//    int block_sizes[] = {32768};  
    // the polynomial needs to be in reverse order, MSB to the right

// https://www.ece.cmu.edu/~koopman/crc/
// http://www.ece.cmu.edu/~koopman/pubs/ray06_crcalgorithms.pdf
// •Koopman, P. & Chakravarty, T., "Cyclic Redundancy Code (CRC) Polynomial Selection For Embedded Networks," DSN04, June 2004
// Table 3 used as reference
// using best polynomials suggested for large block sizes
// note that they are provided in reversed-reciprocal form, so here we convert them to just reversed
// 0xA6 for 8-bit, 0xB75 for 12-bit, 0xBAAD for 16-bit, no info for 24-bit but 0x5D6DCB was listed in one Koopman paper(this one isn't in reverced reciprocal) and this also happens to be FlexRay's poly, 0x1EDC6F41 for 32-bit
    uint64_t polynomials[] = {0xC2,0xD76,0xDAAE,0xD3B6BA,0x82F63B78};
    uint64_t *tables = (uint64_t *) malloc(sizeof(uint64_t) * sizeof(polynomials)/sizeof(polynomials[0]) * 256);
//    uint64_t tables[sizeof(polynomials)/sizeof(polynomials[0])][256];
    FILE *input1;
    input1 = fopen(argv[1], "r");

    FILE *input2;
    input2 = fopen(argv[2], "r");

    if (input1 == NULL) {
        printf("Error opening %s for reading\n", argv[1]);
        return -1;
    }

    if (input2 == NULL) {
        printf("Error opening %s for reading\n", argv[2]);
        return -1;
    }
    int i = 0;
    for (;i<sizeof(polynomials)/sizeof(polynomials[0]);i++) {
	compute_crc_table(polynomials[i]); // initialize crc
        memcpy(&(tables[i*256]),&(table.table[0]),256 * sizeof(table.table[0]));
    }
    
    for (i=0;i<sizeof(block_sizes)/sizeof(block_sizes[0]);i++) {
        compare(input1,input2,block_sizes[i],tables,sizeof(polynomials)/sizeof(polynomials[0]));
        rewind(input1);
        rewind(input2);
    }
    fclose(input1);
    fclose(input2);
}

int compare(FILE *input1, FILE *input2, unsigned int BLOCK_SIZE, uint64_t * tables, unsigned int t_count) {
    uint8_t buffer1[BLOCK_SIZE];
    uint8_t buffer2[BLOCK_SIZE];
    
    unsigned int hamming_distance = 0;
    int i = 0; 
    uint64_t crc1 = 0;
    uint64_t crc2 = 0;
    uint32_t sum1,sum2,sum3,sum4;
    uint64_t fletchers1, fletchers2;    
    unsigned int read1 = 0;
    unsigned int read2 = 0;
    unsigned int blocks_checked = 0;
    unsigned int cc[t_count];
    unsigned int cf[t_count];
    unsigned int ccf[t_count];
    unsigned int cff[t_count];
    unsigned int first = 1;
    memset(cc,0,t_count * sizeof(cc[0]));
    memset(cf,0,sizeof(cf)); 
    memset(ccf,0,sizeof(ccf));
    memset(cff,0,sizeof(cff));
    // since we are comparing crc and fletchers, it is expected that they have the same bit sizes
    int bitsizes[] = {8,12,16,24,32};
// just read and discard one byte at the beginning
//    fread (buffer1, sizeof(buffer1[0]), 1, input1);
//    fread (buffer1, sizeof(buffer1[0]), 1, input2);
    while (0 < (read1 = fread (buffer1, sizeof(buffer1[0]), BLOCK_SIZE, input1))) {
        if (0 < (read2 = fread (buffer2, sizeof(buffer2[0]), BLOCK_SIZE, input2))) {
	    interleave(buffer1,read1*sizeof(buffer1[0]));
	    interleave(buffer2,read2*sizeof(buffer2[0]));
            if (read1 < BLOCK_SIZE || read2 < BLOCK_SIZE) {
                memset(&buffer1[read1],0,sizeof(buffer1[0]) * (BLOCK_SIZE - read1));
                memset(&buffer2[read2],0,sizeof(buffer2[0]) * (BLOCK_SIZE - read2));
            }
	    i = memcmp(buffer1,buffer2, BLOCK_SIZE * sizeof(buffer1[0]));
            //hamming_distance = get_hamming_distance(buffer1, buffer2);            

            if (i) {
                for (i = 0; i < t_count; i++) {
			table.table = &(tables[i*256]);
                        crc1 = crc2 = 0; // reset the crc for each block
                        crc1 = calculate_crc(crc1,buffer1,BLOCK_SIZE);
                        crc2 = calculate_crc(crc2,buffer2,BLOCK_SIZE);

                        if (crc1 == crc2){
                            cc[i]++;
			    if (first) {
			        ccf[i] = 1;
			    }
			}

                        sum1 = sum2 = sum3 = sum4 = 0;
                        fletchers1 = fletchers(&sum1, &sum2, buffer1, BLOCK_SIZE, bitsizes[i]);
                        fletchers2 = fletchers(&sum3, &sum4, buffer2, BLOCK_SIZE, bitsizes[i]);

                        if (fletchers1 == fletchers2) {
                            cf[i]++;
			    if (first) {
                                cff[i] = 1;
                            }

			}

			if ((fletchers1 == fletchers2) && 32 ==  bitsizes[i]) {
				printf ("fl 32 collision - %x\n",fletchers1);
				int j = 0;
				for (j = 0; j < BLOCK_SIZE;j++) {
				//	printf("%02x",buffer1[j]&0xff);
					if ((buffer1[j]&0xff) != (buffer2[j]&0xff))
						printf("%02x %02x at %d\n",buffer1[j]&0xff,buffer2[j]&0xff,j);
				}
				printf("\n");

				//for (j = 0; j < BLOCK_SIZE;j++) {
                                //        printf("%02x",buffer2[j]&0xff);
                                //}
                                //printf("\n");
			}

                }
		first = 0;
                blocks_checked++;
            }
        }
    }
    printf("b%d:%d ",BLOCK_SIZE,blocks_checked);   
    for (i=0;i<t_count;i++) {
	printf("c%d:%d cf%d:%d ",bitsizes[i],cc[i],bitsizes[i],ccf[i]);
    }
    for (i=0;i<t_count;i++) {
        printf("f%d:%d ff%d:%d ",bitsizes[i],cf[i],bitsizes[i],cff[i]);
    }
    printf ("\n");
    return 0;
}
// there is no check to ensure sizes match
unsigned int get_hamming_distance (const uint8_t *i1, const uint8_t *i2) {

    if (0 == memcmp(i1,i2, BLOCK_SIZE * sizeof(i1[0])))
        return 0;

    uint8_t tmp;
    unsigned int hamming_distance = 0;
    unsigned int i = 0;

    for (;i < BLOCK_SIZE;i++) {
        get_hamming_distance_int(&hamming_distance, &tmp, i1[i], i2[i]);
    }

    return hamming_distance;

}

void get_hamming_distance_int (unsigned int *hd, uint8_t *tmp, const uint8_t i1, const uint8_t i2) {
    (*tmp) = i1 ^ i2;
    while ((*tmp)) {
        ++(*hd);
        (*tmp) &= (*tmp) - 1;
    }
}
void interleave (void* buf,unsigned int bytes){
    if (bytes < 1 || bytes % 8 != 0) return;
    uint64_t* b = (uint64_t*)buf;
    int i;
    for (i = 0;i<bytes/sizeof(b[0]);i++){
        b[i] = interleave_64(b[i]);
    }

}

uint64_t interleave_64(uint64_t d) {
        uint64_t d1 = d & 0xffffffff;
        uint64_t d2 = (d >> 32) & 0xffffffff;
        uint64_t result = 0;
        int i = 0;
        for (;i<32;i++) {
                result |= ((d1 & (1U <<i))) << i | ((d2 & (1U << i)) << (i+1));
        }
        return result;
}

