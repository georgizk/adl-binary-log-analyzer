#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "crc.h"

// polynomial in reverse - highest order is LSB.
// the table must have 256 elements
void compute_crc_table(uint64_t polynomial) {
    table.table_type = OTHER;
    table.table = (uint64_t*)malloc(sizeof(uint64_t) * 256);
    uint16_t byte = 0;
    uint8_t i = 0;
    for (byte=0;byte<256;byte++) {
        table.table[byte] = byte;
        // when we're done with the 8 loops, whatever we're left with is the remainder
        for (i=0;i<8;i++) {
            // the polynomial's highest order is implicit - that's why we're shifting the dividend by one before xoring
            if (table.table[byte] & 0x1 ) {
                table.table[byte] = polynomial ^ (table.table[byte]>>1);
            }
            else {
                // bit is 0, so we just shift right
                table.table[byte] = table.table[byte] >> 1;
            }
        }

    }
}
// table needs to have been computed first for this to produce meaningful results
uint64_t calculate_crc(uint64_t crc, const void *buf, size_t size ) {
    const uint8_t *p;
    p = buf;
    // this code has been commented out, but left in since it makes it easier to understand what is happening
    //*uint8_t index = 0;
    //*uint64_t lookup = 0;

    //*while (size--) {
        // the crc is xored as well since it comes from the previous computation
        // example taken from andrewl.dreamhosters.com/filedump/crc64.cpp
        //
        // 1) input:
        //
        //    00 00 00 00 00 00 00 00 AD DE
        //
        // 2) index crc table for byte DE (really for dividend 00 00 00 00 00 00 00 00 DE)
        //
        //    we get A8B4AFBDC5A6ACA4
        //
        // 3) apply that to the input stream:
        //
        //    00 00 00 00 00 00 00 00 AD DE
        //       A8 B4 AF BD C5 A6 AC A4
        //    -----------------------------
        //    00 A8 B4 AF BD C5 A6 AC 09

        // the part where AD is xored with A4 is essentially what we're doing here (we're one step ahead, since we haven't actually xored yet)
        //* index = (*p++ ^ crc) & 0xFF;

        //* lookup = table[index];

        //* crc >>= 8;
        // now we do the actual xoring using the data from the table
        //* crc ^= lookup;
    //*}
        // this can be done more efficiently with the code below
    while (size--)
        crc = table.table[(crc ^ *p++) & 0xFF] ^ (crc >> 8);


    return crc;

}
uint32_t crc32(uint32_t crc, const void *buf, size_t size ) {
/*
    const uint8_t *p;

    p = buf;
    crc = crc ^ ~0U;
    while (size--)
        crc = table[(crc ^ *p++) & 0xFF] ^ (crc >> 8);

    return crc ^ ~0U;

*/
    if (table.table_type != CRC32) {
        printf("Using CRC32\n");
        compute_crc_table(0xEDB88320);
        table.table_type = CRC32;
    }

    // crc32 initializes crc to all 1s,thus the inversions
    return (uint32_t)calculate_crc(crc ^ ~0U, buf,size) ^ ~0U;
}
uint64_t crc64(uint64_t crc, const void *buf, size_t size ) {
    if (table.table_type != CRC64) {
        printf("Using CRC64\n");
        compute_crc_table(0xC96C5795D7870F42ULL);
        table.table_type = CRC64;
    }

    return calculate_crc(crc,buf,size);
}
