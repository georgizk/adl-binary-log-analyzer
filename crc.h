#ifndef CRC_H
#define CRC_H

#include <stdlib.h>
#include <stdint.h>

void compute_crc_table(uint64_t polynomial);
uint32_t crc32(uint32_t crc, const void *buf, size_t size);
uint64_t crc64(uint64_t crc, const void *buf, size_t size);
uint64_t calculate_crc(uint64_t crc, const void *buf, size_t size);

typedef struct
{
    enum {UNINITIALIZED, CRC32, CRC64, OTHER} table_type;
    uint64_t *table;
} crc_table;

crc_table table;

#endif /* CRC_H */
