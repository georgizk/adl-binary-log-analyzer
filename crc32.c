#include <stdio.h>
#include <stdlib.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "crc.h"

static const unsigned int BLOCK_SIZE = 1024;

int main(int argc, char *argv[])
{
    if (argc != 2){
        printf("Usage: %s [input file]\n", argv[0]);
        return -1;
    }

    FILE *input;
    input = fopen(argv[1], "r");


    if (input == NULL) {
        printf("Error opening %s for reading\n", argv[1]);
        return -1;
    }
   
    uint64_t crc = 0;
    uint8_t buffer[BLOCK_SIZE];
    unsigned int read = 0;

    while (0 < (read = fread (buffer, sizeof(buffer[0]), BLOCK_SIZE, input))) {
       // printf("%d bytes read\n",read);
        crc = crc32(crc,buffer,read);
    }
    
    printf("%" PRIx64 "\n",crc);
    return 0;
}
