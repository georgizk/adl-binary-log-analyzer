#include <stdint.h>
#include "fingerprinting_buffer.h"
#include <stdlib.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

void buf_init() {
    fbuffer.length = 0;
    fbuffer.capacity = 0;
    fbuffer.data = NULL;
    fbuffer.head = NULL;
    buf_allocate(STARTING_CAPACITY);
}

void buf_allocate(unsigned int elements) {
    unsigned int old_capacity = fbuffer.capacity;
    uint32_t * old_data = fbuffer.data;
    uint32_t * new_data;
    unsigned int i = 0;
    new_data = (uint32_t*) malloc( sizeof(uint32_t) * elements);
    memset( new_data, 0, sizeof(uint32_t) * elements );
    if (new_data == NULL) {
        printf("Failed to allocate %d bytes",sizeof(uint32_t) * elements);
        return;
    }
    if (old_data != NULL) {
        memcpy(new_data, fbuffer.head, sizeof(uint32_t) * (old_capacity - (fbuffer.head - old_data) ));
        memcpy(&new_data[old_capacity - (fbuffer.head - old_data)], old_data, sizeof(uint32_t) * (fbuffer.head - old_data));

        free(old_data);
    }
    fbuffer.head = new_data;
    fbuffer.data = new_data;
    fbuffer.capacity = elements;
}

void buf_push (uint32_t* element) {

    if (fbuffer.length >= fbuffer.capacity) {
        buf_allocate(fbuffer.capacity * 2); // assume this worked...
    }

    // index can wrap around
    fbuffer.data[( fbuffer.length+(fbuffer.head-fbuffer.data) ) % fbuffer.capacity] = *element;

    fbuffer.length++;    
}

uint32_t buf_pop() {
    // increment the head pointer... if we're at the end of allocated memory, wrap around
    if (fbuffer.length > 0) {    
        fbuffer.length--;
        if ((fbuffer.head - fbuffer.data) < fbuffer.capacity - 1) {
            fbuffer.head++;  
            return * (fbuffer.head-1);            
        }
        else {
            fbuffer.head = fbuffer.data;
            return * (fbuffer.head+fbuffer.capacity - 1);
        }  
    }
    
    return 0;    
}
