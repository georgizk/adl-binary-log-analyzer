#ifndef FINGERPRINTING_BUFFER_H
#define FINGERPRINTING_BUFFER_H

#include <stdint.h>

const static unsigned int STARTING_CAPACITY = 4;
typedef struct
{
    uint32_t * data; /* pointer to beginning of where the data is stored */
    unsigned int capacity; /* the capacity of the buffer. this can vary */
    unsigned int length; /* the number of elements currently stored */
    uint32_t * head; /* this points to the head of the buffer, i.e. next element to be read */
} fingerprinting_buffer;

fingerprinting_buffer fbuffer;

// initialize buffer
void buf_init();
// push an element to the buffer
void buf_push (uint32_t* element);
// remove an element from the buffer
uint32_t buf_pop();
// allocate memory
void buf_allocate(unsigned int elements);

#endif /* FINGERPRINTING_BUFFER_H */
