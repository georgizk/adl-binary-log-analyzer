#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "fletchers.h"
char tmp_bits;
uint64_t
fletchers(uint32_t *sum1, uint32_t *sum2, const void *buf, size_t size, char bits)
{
	tmp_bits = bits;
    const uint8_t *p;
    const uint16_t *p2;
    uint32_t tmp;
    unsigned int i = 0;
//    if (0 == modulo) {
        modulo = (1 << bits/2) - 1;
//    }
    p = buf;
    p2 = buf;
    while (size--) {
        if (bits == 8) {
		tmp = (*p & 0xF0) >> 4;
		accumulate(tmp & 0x0F,sum1,sum2);
		tmp = *p & 0x0F;
		accumulate(tmp & 0x0F,sum1,sum2);
	}
        else if (bits == 12) {
		
        	tmp = *p; // tmp = xxxxxxzz
		tmp = tmp >> 2; // tmp = 00xxxxxx
		accumulate (tmp & 0x3F,sum1,sum2); // first 6 bits done... we want 24 total, since 24 is divisible by 6 and 8

		tmp = *p & 0x3; // tmp = 000000xx
		tmp = tmp << 8; // tmp = xx00000000
		// the two remaining bits will be msb of the next 6 bits

		*p++;
		size--; // get the next byte

		tmp = tmp | *p; // tmp = xxxxxxzzzz
		tmp = tmp >> 4; // tmp = 00xxxxxx
		accumulate (tmp & 0x3F,sum1,sum2); // second 6 bits done

		tmp = *p & 0x0F; // tmp = 0000xxxx
		tmp = tmp << 8; // tmp = xxxx00000000
		// the 4 remaining bits will be msb of next 2 bits

		*p++;
                size--; // get the next byte

		tmp =  tmp | *p; // tmp = xxxxxxzzzzzz
		tmp = tmp >> 6; // tmp = 00xxxxxx

		accumulate (tmp & 0x3F,sum1,sum2); // third 6 bits done

		tmp = *p; // tmp = zzxxxxxx
		accumulate (tmp & 0x3F,sum1,sum2); // last 6 bits done
	}
        else if (bits == 24) {
		tmp = (*p) << 4;
		size--;
                *p++;
                i++;
		tmp = tmp | (((*p) & 0xF0) >> 4);
		accumulate(tmp & 0x0FFF,sum1,sum2);
		tmp = ((*p) & 0xF) << 8;
		size--;
                *p++;
                i++;
		tmp = tmp | *p;
		accumulate(tmp & 0x0FFF,sum1,sum2);
	}
	else if (bits == 32) {
		tmp = *p;
		tmp = tmp << 8;
		size--;
		*p++;
		tmp = tmp | *p;
		accumulate(tmp & 0xFFFF,sum1,sum2);	
	}
	else {
                
        	accumulate(*p,sum1,sum2);
        }
	*p++;
    }
//printf ("\n\ns=%" PRIu32 " t=%" PRIu32 "\n",*sum1, *sum2);
    return *sum1 | (*sum2)<<(bits/2);
}

void accumulate(uint32_t val, uint32_t *sum1, uint32_t *sum2) {
	//printf("%u, ",val);
//printf ("\n\ns=%" PRIu32 " t=%" PRIu32 "\n",*sum1, *sum2);
/*	uint32_t tmp = val;
	int i = 0;
	int seen = 0;
	while (i < 32) {
		if (tmp & (0x1 << 31)) {
			printf ("1");
			seen = 1;
		}
		else if (seen || i > (31 - (tmp_bits/2 ))){
			printf("0");
		}
		tmp = tmp << 1;
		i++;
	}*/
	*sum1 = (*sum1 + val) % modulo;
        *sum2 = (*sum1 + *sum2) % modulo;
}
uint64_t
fletchers32(uint32_t *sum1, uint32_t *sum2, const void *buf, size_t size) {
   modulo = 0;

//   return fletchers(sum1,sum2,buf,size,32);
const uint8_t *p;
p = buf;
    while (size--) {
        *sum1 = (*sum1 + *p++) % 65535;
        *sum2 = (*sum1 + *sum2) % 65535;
    }
return *sum1 | (*sum2)<<16;
}

uint32_t fletcher32(const void *buf, size_t words )
{
        uint32_t sum1 = 0, sum2 = 0;
 	const uint16_t *data;
        data = buf;
        while (words) {
                unsigned tlen = words > 359 ? 359 : words;
                words -= tlen;
                do {
                        sum2 += sum1 += *data++;
                } while (--tlen);
                sum1 = (sum1 & 0xffff) + (sum1 >> 16);
                sum2 = (sum2 & 0xffff) + (sum2 >> 16);
        }
        /* Second reduction step to reduce sums to 16 bits */
        sum1 = (sum1 & 0xffff) + (sum1 >> 16);
        sum2 = (sum2 & 0xffff) + (sum2 >> 16);
        return sum2 << 16 | sum1;
}

