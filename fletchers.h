#ifndef FLETCHERS_H
#define FLETCHERS_H

uint64_t
fletchers(uint32_t *sum1, uint32_t *sum2, const void *buf, size_t size, char bits);
uint64_t
fletchers32(uint32_t *sum1, uint32_t *sum2, const void *buf, size_t size);
uint32_t fletcher32( const void *data, size_t words );

uint32_t modulo = 0;

void accumulate(uint32_t val, uint32_t *sum1, uint32_t *sum2);
#endif /* FLETCHERS_H */
