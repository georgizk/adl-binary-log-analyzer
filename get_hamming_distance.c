#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "get_hamming_distance.h"
#include "crc.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

int main(int argc, char *argv[]) {
    if (argc < 4){
        printf("Usage: %s [binary file 1] [binary file 2] [block size] [polynomial] (fault at byte) \n", argv[0]);
        return -1;
    }
    
    BLOCK_SIZE = atoi(argv[3]);    
    
    // the polynomial needs to be in reverse order, MSB to the right
    uint64_t polynomial = 0xEDB88320;
    uint64_t tmp;
    if (sscanf (argv[4],"0x%" PRIX64,&tmp))
        polynomial = tmp;
    
    if (argc > 5) {
        INSERTED_AT = atoi(argv[5]) / BLOCK_SIZE;
    }   
        
    FILE *input1;
    input1 = fopen(argv[1], "r");

    FILE *input2;
    input2 = fopen(argv[2], "r");

    if (input1 == NULL) {
        printf("Error opening %s for reading\n", argv[1]);
        return -1;
    }

    if (input2 == NULL) {
        printf("Error opening %s for reading\n", argv[2]);
        return -1;
    }
    compute_crc_table(polynomial); // initialize crc

    printf ("%X,",polynomial);
    printf ("%d,",BLOCK_SIZE);
    
    uint8_t buffer1[BLOCK_SIZE];
    uint8_t buffer2[BLOCK_SIZE];
    
    unsigned int block_number = 0;
    unsigned int first_block_hamming_distance = 0;
    
    uint64_t crc1 = 0;
    uint64_t crc2 = 0;    
    unsigned int hamming_distance = 0;
    long unsigned int total_hamming_distance = 0;
    int error_detection_latency = -1;
    int error_missed = 0;
    int error_missed_global = 0;
    unsigned int read1 = 0;
    unsigned int read2 = 0;
    while (0 < (read1 = fread (buffer1, sizeof(buffer1[0]), BLOCK_SIZE, input1))) {
        if (0 < (read2 = fread (buffer2, sizeof(buffer2[0]), BLOCK_SIZE, input2))) {
            if (read1 < BLOCK_SIZE || read2 < BLOCK_SIZE) {
                memset(&buffer1[read1],0,sizeof(buffer1[0]) * (BLOCK_SIZE - read1));
                memset(&buffer2[read2],0,sizeof(buffer2[0]) * (BLOCK_SIZE - read2));
            }
            hamming_distance = get_hamming_distance(buffer1, buffer2);            
            if (hamming_distance > 0) {           
                total_hamming_distance += hamming_distance;
                crc1 = crc2 = 0; // reset the crc for each block
                crc1 = calculate_crc(crc1,buffer1,read1);
                crc2 = calculate_crc(crc2,buffer2,read2);
                
                if (first_block_hamming_distance == 0) {
                    first_block_hamming_distance = hamming_distance;
                    if (crc1 == crc2)
                        error_missed = 1;
                }
              
                if ((crc1 == crc2) && (hamming_distance > 0)) {
                    error_missed_global = 1;
                    // printf("Error Missed - fingerprint collision! Dumping blocks!\n");
                    // printf("\nLeft Block:\n");             
                    // for (crc1 = 0; crc1 < read1; crc1++) {
                        // printf("%" PRIx8,buffer1[crc1]);  
                    // }   
                    // printf("\n\nRight Block:\n");             
                    // for (crc1 = 0; crc1 < read2; crc1++) {
                        // printf("%" PRIx8,buffer2[crc1]);  
                    // }    
                    // printf("\n\n");
                
                }
                if ((-1 == error_detection_latency) && (crc1 != crc2))
                    error_detection_latency = block_number - INSERTED_AT;
            }
            
            block_number++;   
        }
    }
    
    printf("%" PRIx64 ",%" PRIx64 ",%d,%d,%d,%d,%d,",crc1,crc2,error_detection_latency,first_block_hamming_distance,total_hamming_distance,error_missed,error_missed_global);
    // printf("The error detection latency was %d blocks, blocksize was %d bytes\n",error_detection_latency,BLOCK_SIZE);
    // if (error_missed_global) {
         // printf("An Error was missed! Please refer to log.\n");
    // }
    fclose (input1);
    fclose (input2);
    return 0;
}
// there is no check to ensure sizes match
unsigned int get_hamming_distance (const uint8_t *i1, const uint8_t *i2) {

    if (0 == memcmp(i1,i2, BLOCK_SIZE * sizeof(i1[0])))
        return 0;

    uint8_t tmp;
    unsigned int hamming_distance = 0;
    unsigned int i = 0;

    for (;i < BLOCK_SIZE;i++) {
        get_hamming_distance_int(&hamming_distance, &tmp, i1[i], i2[i]);
    }

    return hamming_distance;

}

void get_hamming_distance_int (unsigned int *hd, uint8_t *tmp, const uint8_t i1, const uint8_t i2) {
    (*tmp) = i1 ^ i2;
    while ((*tmp)) {
        ++(*hd);
        (*tmp) &= (*tmp) - 1;
    }
}
