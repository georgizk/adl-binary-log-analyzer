#ifndef GET_HAMMING_DISTANCE_H
#define GET_HAMMING_DISTANCE_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

unsigned int get_hamming_distance (const uint8_t *i1, const uint8_t *i2);
void get_hamming_distance_int (unsigned int *hd, uint8_t *tmp, const uint8_t i1, const uint8_t i2);

static unsigned int BLOCK_SIZE = 800;
static unsigned int INSERTED_AT = 0;

#endif /* GET_HAMMING_DISTANCE_H */
