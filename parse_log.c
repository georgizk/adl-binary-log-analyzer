#include <stdio.h>
#include <stdint.h>
#include "parse_log.h"
#include "fingerprinting_buffer.h"
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc < 3){
        printf("Usage: %s [ADL log to parse] [output file] (instruction number) (comparison log)\n", argv[0]);
        return -1;
    }    
    int result = 0; 
    if (argc < 4) {
        result = parse_log(argv[1],argv[2],"0\0","0\0");    
    }
    else {
        result = parse_log(argv[1],argv[2],argv[3],argv[4]);    
    }
    printf("%d %d\n", result, fbuffer.capacity);
    
    return result;
}

int parse_log(const char* inputFile, const char* outputFile, const char* instrNr, const char* comparisonLog) {          
    FILE *input;
    if (0 != strcmp ("stdin",inputFile))
        input = fopen(inputFile, "r");
    else
        input = stdin;

    lines.length = 0;
    lines.capacity = 0;
    lines.lines = NULL;

    if (input == NULL) {
        printf("Error opening %s for reading\n", inputFile);
        return -1;
    }
    
    FILE *output;
    output = fopen(outputFile, "wb");

    if (output == NULL) {
        printf("Error opening %s for writing\n", outputFile);
        return -1;
    }

    FILE *input_log;
    input_log = fopen(comparisonLog,"r");
    uint32_t comparison_buffer[2];
    unsigned int latency = -1;

    int instr = atoi(instrNr);
    unsigned int bytes_written = 0;
    int inst_at_byte = -1;
    int parse_line_result = 0;
    buf_init();
    
    char line [ LINE_BUFFER ];
    parse_config(line);
    
    unsigned int i = 0, j = 0;
    uint32_t buffer[WRITE_BUFFER_SIZE];

    while ( fgets ( line, sizeof(line), input ) != NULL ) /* read a line */
    {
        if (i == WRITE_BUFFER_SIZE) {
            fwrite (buffer, sizeof(buffer[0]), WRITE_BUFFER_SIZE, output);
            i = 0;
        }
        if (-1 == inst_at_byte && parse_line_result > instr)
                inst_at_byte = bytes_written + fbuffer.length * sizeof(buffer[0]); 
 
        // got in instruction i.e. CPU cycle - process data in buffer
        if (0 != (parse_line_result = parse_line (line,&j,&(buffer[i]))) && (fbuffer.length > 0)) {
            // fingerprint only processes data if there's something in the buffer - 32 or 64 bits at a time
            if (FINGERPRINTED_BITS_PER_CYCLE == 64) {
                buffer[i++] = buf_pop();
                buffer[i++] = buf_pop();  // if nothing for 2nd pop, it's just zeroes   
                bytes_written += sizeof(buffer[0]) * 2;

                if ( (NULL != input_log) && (-1 == latency) ) {
                    if (2 == fread (comparison_buffer, sizeof(comparison_buffer[0]), 2, input_log) ) {
                         if ( (comparison_buffer[0] != buffer[i-2]) || (comparison_buffer[1] != buffer[i-1]) ) {
                              latency = parse_line_result - instr;
                         }
                    }
                }
            } 
            else if (FINGERPRINTED_BITS_PER_CYCLE == 32) {
                buffer[i++] = buf_pop(); 
                bytes_written += sizeof(buffer[0]);
            }
        }
    }
    // flush the fingerprinting buffer
    while (fbuffer.length > 0) {
        buffer[i++] = buf_pop();
        bytes_written += sizeof(buffer[0]);
        if (i == WRITE_BUFFER_SIZE) {
            fwrite (buffer, sizeof(buffer[0]), WRITE_BUFFER_SIZE, output);
            i = 0;
        }        
    }
    // flush whatever is left in the output buffer
    fwrite (buffer, sizeof(buffer[0]), i, output); 

    fclose (output);
    fclose (input);
    printf("%d ",latency);
    return inst_at_byte;
}
int parse_config (char* line) {
    FILE *config;
    config = fopen(CONFIG_FILE_PATH, "r");
    
    if (config == NULL) {
        printf("Config file %s not found\n", CONFIG_FILE_PATH);
        return -1;
    }
    
    char parsing_lines = 0;
    while ( fgets ( line, WRITE_BUFFER_SIZE * sizeof(char), config ) != NULL ) /* read a line */
    {
        if (0 == strcmp(line,LINES_TRIGGER))
            parsing_lines = 1;
            
        if (line[0] != '#' && line[0] != '\n' && parsing_lines == 1) {
            add_line(line);
        }
    }
}
uint32_t parse_line (const char* line,  unsigned int* i, uint32_t* data) {
    // if instruction fetch, return the cycle number
    if (line[0] == I_INSTR[0] && 1 == sscanf (line, I_INSTR, data)) {      
        return *data;        
    }
    
    // parse for parseable data
    for (*i = 0; *i < lines.length; (*i)++) {
        // the first character comparison actually has a noticeable impact on performance - about 25% slower without it
        if(line[0] == lines.lines[*i][0] && 1 == sscanf (line, lines.lines[*i], data)) 
            buf_push(data);
    
    }
    // return 0 since it wasn't an instruction fetch
    return 0;
}

void array_allocate(unsigned int elements){
    unsigned int old_capacity = lines.capacity;
    char ** old_lines = lines.lines;
    char ** new_lines;
    unsigned int i = 0;
    if (elements == old_capacity) 
	elements = 1;
    new_lines = (char**) malloc( sizeof(char*) * elements);    
    
    if (new_lines == NULL) {
        printf("Failed to allocate %d bytes",sizeof(char*) * elements);
        return;
    }    
    for (i = 0; i < elements; i++) {
        new_lines[i] = malloc(LINE_BUFFER * sizeof(char));
    }
    if (old_lines != NULL) {
        for (i = 0; i < old_capacity; i++) {
            memcpy(new_lines[i], old_lines[i], sizeof(char) * LINE_BUFFER);
            free (old_lines[i]);
        }
        free(old_lines);
    }
    lines.lines = new_lines;
    lines.capacity = elements;
}
void add_line(char* line) {
    if (lines.length >= lines.capacity) {
        array_allocate(lines.capacity * 2);
    }
    memcpy(lines.lines[lines.length], line, sizeof(char) * LINE_BUFFER);
    lines.length++;
}

