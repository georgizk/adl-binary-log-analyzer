#ifndef PARSE_LOG_H
#define PARSE_LOG_H

#include <stdint.h>

typedef struct
{
    char ** lines; /* pointer to beginning of lines */
    unsigned int capacity; /* the maximum number of lines the buffer could store */
    unsigned int length; /* the number of lines currently stored */
} lines_to_parse;

lines_to_parse lines;

static const unsigned int WRITE_BUFFER_SIZE = 1024;
static const unsigned int FINGERPRINTED_BITS_PER_CYCLE = 64;
static const unsigned int LINE_BUFFER = 256; 

static const char CONFIG_FILE_PATH[] = "parse_log.config"; 
static const char LINES_TRIGGER[] = "[lines]\n"; 
static const char I_INSTR[] = "I ea=0x%*s id=%*s tic=%d";

void array_allocate(unsigned int elements);
void add_line(char* line);
int parse_config (char* line);
uint32_t parse_line (const char* line,  unsigned int* i, uint32_t* data);
int parse_log(const char* inputFile, const char* outputFile, const char* instrNr, const char* comparisonLog);

#endif /* PARSE_LOG_H */
